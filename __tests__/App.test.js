import React from 'react';
import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import App from '../src/App';
import * as API from '../src/api/Main.connector';

describe('App view', () => {
    test('App should render with default fashion checked', async () => {
        jest.spyOn(API, 'fetchFashionsArticles').mockResolvedValue(mockFashionResponse);
        render(<App />);
        const articles = await screen.findAllByTestId('article');
        expect(articles.length).toBe(4);
        cleanup();
    });

    test('App sould render error info if API return error', async () => {
        jest.spyOn(API, 'fetchFashionsArticles').mockRejectedValue(mockErrorResponse);
        render(<App />);
        const articles = await screen.findByText('Articles not found');
        expect(articles).toBeDefined();
        cleanup();
    });

    test('App should fetch sports data if user checked sport checkbox', async () => {
        jest.spyOn(API, 'fetchFashionsArticles').mockResolvedValue(mockFashionResponse);
        jest.spyOn(API, 'fetchSportsArticles').mockResolvedValue(mockSportResponse);
        render(<App />);
        const sportCheckbox = screen.getByTestId('sportCheckbox');
        fireEvent.click(sportCheckbox);
        const sportArticle = await screen.findByText(
            'Vålerengas førsterekke smadrer rivalene: - Seriegullet er I våre hender'
        );
        expect(sportArticle).toBeDefined();
    });
});

const mockFashionResponse = {
    articles: [
        {
            id: 157489,
            date: '5. februar 2019',
            image: 'https://placeimg.com/300/180/arch',
            category: 'fashion',
            title: 'Kongen stilte i Moon Boots: – Helt konge!',
            preamble: 'Kong Harald (81) får moteskryt for spenstig skovalg.'
        },

        {
            id: 872133,
            date: '13. desember 2018',
            image: 'https://placeimg.com/320/200/any',
            category: 'fashion',
            title: 'Cara Delevingne gjenskaper Janet Jacksons toppløsbilde for Balmain',
            preamble: 'Se de dristige bildene av supermodellen.'
        },
        {
            id: 432351,
            date: '8. november 2018',
            image: 'https://placeimg.com/320/200/any',
            category: 'fashion',
            title: '10 varme gensere til kalde vintermåneder',
            preamble:
                'Jula varer helt til påske, og det samme gjør minusgradene. Her er genserne vi vil pakke oss inn i denne vinteren.'
        },

        {
            id: 986521,
            date: '7. februar 2019',
            image: 'https://placeimg.com/280/200/any',
            category: 'fashion',
            title: 'Bør du følge smokingreglene?',
            preamble:
                'Det finnes mange skrevne og uskrevne regler som beskriver hva en smoking egentlig er. Vi har tatt en titt på hvilke du bør følge, og hvilke du kan bryte.'
        }
    ]
};

const mockSportResponse = {
    articles: [
        {
            id: 789702,
            date: '2. februar 2019',
            image: 'https://placeimg.com/280/180/nature',
            category: 'sport',
            title: 'Vålerengas førsterekke smadrer rivalene: - Seriegullet er I våre hender',
            preamble:
                'MERÅKER (VG) Finn-Hågen Krogh (28) opplevde den gedigne nedturen da han ble vraket til OL-sprinten i Sotsji etter at han først var tatt ut på laget. Nå føler han seg aldri trygg på å få starte i mesterskap.'
        },
        {
            id: 123544,
            date: '1. oktober 2018',
            image: 'https://placeimg.com/280/180/nature',
            category: 'sport',
            title: 'Solskjær fikk klar beskjed fra Røkke og Gjelsten: – Ikke kom tilbake!',
            preamble: 'Ole Gunnar Solskjær forteller om den spesielle samtalen med de to Molde-investorene.'
        },
        {
            id: 157489,
            date: '5. februar 2019',
            image: 'https://placeimg.com/300/180/arch',
            category: 'fashion',
            title: 'Kongen stilte i Moon Boots: – Helt konge!',
            preamble: 'Kong Harald (81) får moteskryt for spenstig skovalg.'
        },
        {
            id: 126432,
            date: '24. mai 2018',
            image: '',
            category: 'sport',
            title: 'Dortmund drar fra etter overraskende Bayern-tap',
            preamble:
                'Selv om Borussia Dortmund bare klarte 1-1 borte mot Eintracht Frankfurt, fikk de god hjelp av Leverkusen som slo Bayern München 3-1 i tysk bundesliga.'
        },
        {
            id: 872133,
            date: '13. desember 2018',
            image: 'https://placeimg.com/320/200/any',
            category: 'fashion',
            title: 'Cara Delevingne gjenskaper Janet Jacksons toppløsbilde for Balmain',
            preamble: 'Se de dristige bildene av supermodellen.'
        },
        {
            id: 432351,
            date: '8. november 2018',
            image: 'https://placeimg.com/320/200/any',
            category: 'fashion',
            title: '10 varme gensere til kalde vintermåneder',
            preamble:
                'Jula varer helt til påske, og det samme gjør minusgradene. Her er genserne vi vil pakke oss inn i denne vinteren.'
        },
        {
            id: 846253,
            date: '19. september 2018',
            image: 'https://placeimg.com/320/200/any',
            category: 'sport',
            title: 'Kraftig snøvær avlyser VM-generalprøven',
            preamble: null
        },
        {
            id: 986521,
            date: '7. februar 2019',
            image: 'https://placeimg.com/280/200/any',
            category: 'fashion',
            title: 'Bør du følge smokingreglene?',
            preamble:
                'Det finnes mange skrevne og uskrevne regler som beskriver hva en smoking egentlig er. Vi har tatt en titt på hvilke du bør følge, og hvilke du kan bryte.'
        },
        {
            id: 122866,
            date: '15. januar 2019',
            image: 'https://placeimg.com/280/180/any',
            category: 'sport',
            title: 'Her får lillebror gode råd av Petter Northug',
            preamble: 'Før NM-sprinten i fristil i Grova i Meråker, fikk Even Northug gode råd av storebror Petter.'
        }
    ]
};

const mockErrorResponse = { message: "Ooooops! this shouldn't be shown to clients probably..." };
