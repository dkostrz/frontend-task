const API_URL = 'http://localhost:6010';

export const getHTTP = async (url) => {
    const response = await fetch(`${API_URL}${url}`);
    if (!response.ok) {
        const err = await response.json();
        console.log(err);
        throw new Error(err);
    }
    const res = await response.json();
    return res;
};
