import { getHTTP } from './API';

export const fetchFashionsArticles = () => {
    return getHTTP(`/articles/fashion`);
};

export const fetchSportsArticles = () => {
    return getHTTP(`/articles/sports`);
};
