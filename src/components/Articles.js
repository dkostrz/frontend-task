import React from 'react';

export const Articles = ({ articles }) => {
    return (
        <div>
            {articles.map((item) => (
                <article data-testid="article" className="article flexbox" key={item.id}>
                    <img
                        className="article__image"
                        src={item.image}
                        onError={(e) => {
                            e.target.src = '/img/image-not-found.jpg';
                        }}
                    />
                    <div className="flex-1">
                        <div className="flexbox--md flexbox flexbox--align-center flexbox--sbet">
                            <h2 className="article__title">{item.title}</h2>

                            <div className="">{item.date}</div>
                        </div>
                        <p className="article__premable">{item.preamble}</p>
                    </div>
                </article>
            ))}
            {articles.length == 0 && (
                <div className="text-center f-size-30">
                    <strong>Articles not found</strong>
                </div>
            )}
        </div>
    );
};
