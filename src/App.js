import React, { useEffect, useState } from 'react';
import { Articles } from './components/Articles';
import { fetchFashionsArticles, fetchSportsArticles } from './api/Main.connector';

const App = () => {
    const [articlesList, setArticlesList] = useState([]);
    const [isFashionChecked, setIsFashionChecked] = useState(true);
    const [isSportsChecked, setIsSportsChecked] = useState(false);
    const [error, setError] = useState(undefined);

    useEffect(() => {
        (async () => {
            try {
                let fashions = [];
                let sports = [];
                if (isFashionChecked) {
                    const resFash = await fetchFashionsArticles();
                    fashions = resFash.articles;
                    setError(undefined);
                }

                if (isSportsChecked) {
                    const resSport = await fetchSportsArticles();
                    sports = resSport.articles;
                    setError(undefined);
                }

                const all = [];
                const concated = all.concat(fashions, sports);
                setArticlesList(concated);

                if (!isFashionChecked && !isSportsChecked) {
                    setArticlesList([]);
                }
            } catch (e) {
                setError('Error with API');
            }
        })();
    }, [isSportsChecked, isFashionChecked]);

    function sort(a, b) {
        const dateA = new Date(a.date);
        const dateB = new Date(b.date);
        return dateA > dateB ? 1 : -1;
    }

    function sortArticles(type) {
        const art = [...articlesList];
        if (type == 'DESC') {
            art.sort(sort);
            setArticlesList(art);
        } else {
            art.sort(sort);
            setArticlesList(art);
        }
    }

    return (
        <main className="container flexbox--md flexbox mt-100">
            <aside className="flexbox flexbox--col flex-1">
                <div>Data sources</div>
                <label className="mt-30">
                    <input
                        type="checkbox"
                        defaultChecked={isFashionChecked}
                        onChange={(e) => setIsFashionChecked(e.target.checked)}
                    />{' '}
                    Fashion
                </label>
                <label>
                    <input
                        data-testid="sportCheckbox"
                        type="checkbox"
                        defaultValue={isSportsChecked}
                        onChange={(e) => setIsSportsChecked(e.target.checked)}
                    />{' '}
                    Sports
                </label>
            </aside>

            <section className="flex-4">
                <div className="flexbox flexbox--end flexbox--align-center">
                    <div>Sort by date</div>
                    <div className="flexbox flexbox--col ml-20">
                        <span className="arrow arrow--up" onClick={() => sortArticles('DESC')}></span>
                        <span className="arrow arrow--down" onClick={() => sortArticles('ASC')}></span>
                    </div>
                </div>
                {error ? <div>{error}</div> : <Articles articles={articlesList} />}
            </section>
        </main>
    );
};

export default App;
