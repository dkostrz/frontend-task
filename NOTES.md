The task was very simple for me. I made all assumptions. For the task, I used one library: ReactJS to speed up the creation of views. I used fetch () to connect to the API because it is a built-in method in the Web API and is sufficient for a simple GET. I only used useState hook because the logic of the application is so simple that it does not require the use of a state management tool. For styling I used SCSS and BEM methodology.

The project also includes tests using Jest and React Testing Library

The application can be improved by adding TypeScript and library for state management (MobX or Redux) if the application logic becomes more complicated
